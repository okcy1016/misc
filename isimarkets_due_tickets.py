import requests, json

# jira
params = ((
    'jql',
    'project = CDP AND issuetype in (Backfill, Bug, "Fine tune", Maintenance, "New CAP", "Request for new data") AND resolution = Fixed AND assignee in (currentUser()) AND (resolutiondate >= startOfMonth() and resolutiondate <= endOfMonth()) order by updated DESC'
), )

response = requests.get('https://securities.jira.com/rest/api/2/search',
                        params=params,
                        auth=('gchen@isimarkets.com',
                              '4BpTAvqEvzAFZdFXh19c51F6')).json()

closed_amount = len(response['issues'])
due_amount = 27 - closed_amount

# trello
url = "https://api.trello.com/1/cards/61c5895db4346c40d6e12e65?key=cd23511b6b9ffd68b39b11d13c9be501&token=3b4c2c815cad275eec7ce658c43cf3025f24d1231e40c07a7fe393512687a1f6&name=ok"
response2 = requests.put(url.replace("=ok", "=due-tickets " + str(due_amount)))
print(json.dumps({"trellocode": response2.status_code}))
