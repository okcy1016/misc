from wsgiref.simple_server import make_server
import os, json, requests, falcon
import pysnooper


# Falcon follows the REST architectural style, meaning (among
# other things) that you think in terms of resources and state
# transitions, which map to HTTP verbs.
class ThingsResource:
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200  # This is the default status
        resp.content_type = falcon.MEDIA_TEXT  # Default is JSON, so override
        resp.text = ('\nTwo things awe me most, the starry sky '
                     'above me and the moral law within me.\n'
                     '\n'
                     '    ~ Immanuel Kant\n\n')


class TrelloCardsGetAllResource:
    def __init__(self):
        # okcy1016@gmail.com
        self.secret_key = "cd23511b6b9ffd68b39b11d13c9be501"
        self.token = "3b4c2c815cad275eec7ce658c43cf3025f24d1231e40c07a7fe393512687a1f6"
        self.default_board_id = "5ddbc14c7f022f6edf8083c4"
        self.excluded_list_ids = ["5ddbc14d74a5c10fd2e0da1b"]

    def get_lists_in_board(self, board_id=None):
        if not board_id:
            board_id = self.default_board_id

        endpoint = "https://api.trello.com/1/boards/{}/lists?key={}&token={}".format(
            board_id, self.secret_key, self.token)
        transient_data = requests.get(endpoint).json()
        assert (isinstance(transient_data, list))

        lists = []
        for trello_list in transient_data:
            _id = trello_list['id']
            name = trello_list['name']
            if _id in self.excluded_list_ids:
                continue
            lists.append({"id": _id, "name": name})

        return lists

    # @pysnooper.snoop()
    def get_cards_in_list(self, trlist):
        endpoint = "https://api.trello.com/1/lists/{}/cards?key={}&token={}".format(
            trlist['id'], self.secret_key, self.token)

        transient_data = requests.get(endpoint).json()
        assert (isinstance(transient_data, list))

        cards = []
        for card in transient_data:
            card_name = card['name']
            card_id = card['id']
            card_id_list = trlist['id']
            card_name_list = trlist['name']
            cards.append({
                "card_name": card_name,
                "card_id": card_id,
                "card_id_list": card_id_list,
                "card_name_list": card_name_list,
            })

        return cards

    def on_get(self, req, resp):
        data = {'data': []}
        lists = self.get_lists_in_board()

        for trlist in lists:
            cards = self.get_cards_in_list(trlist)
            for card in cards:
                data['data'].append({'type': 'trello_card', 'entity': card})

        resp.text = json.dumps(data)


# falcon.App instances are callable WSGI apps
# in larger applications the app is created in a separate file
app = falcon.App()

# Resources are represented by long-lived class instances
things = ThingsResource()
trello_cards_getall = TrelloCardsGetAllResource()

# things will handle all requests to the '/things' URL path
app.add_route('/things', things)
app.add_route('/trello/cards', trello_cards_getall)

if __name__ == '__main__':
    # heroku container env will have PORT set
    # default to 8000 when local dev
    port = int(os.environ.get('PORT', 8000))
    with make_server('', port, app) as httpd:
        print('Serving on port {}...'.format(port))

        # Serve until process is killed
        httpd.serve_forever()
