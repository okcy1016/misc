from PIL import Image, ImageDraw, ImageFont

class DisplayCell:
    def __init__(self, width, height, text, fn_name, margin=(3,3), fn_size=12):
        self.width = width
        self.height = height
        self.text = text
        self.margin = margin
        self.fn_size = fn_size
        self.font = ImageFont.truetype(fn_name, self.fn_size)
        self.x = None
        self.y = None

    def draw(self, context):
        context.multiline_text((self.x, self.y), self.text, font=self.font)

class Display:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.dsp_cell_width = 
        self.pages = {}
        self.rows = self.get_rows()
        self.columns = self.get_columns()
        self.dsp_cells = []

    def render(self):
        pass

    def get_rows(self):
        return int(self.width / self.cells

    def get_columns(self):
        pass

    def add_display_cell(self):
        pass

class Dis
