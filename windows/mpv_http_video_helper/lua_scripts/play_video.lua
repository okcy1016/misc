local hex_to_char = function(x) return string.char(tonumber(x, 16)) end
local unescape = function(url) return url:gsub("%%(%x%x)", hex_to_char) end

local video_url_str = "\"" .. unescape(video_url) .. "\""
local proxy_cmd = "--http-proxy=http://127.0.0.1:10809"

print("playing url: ", video_url_str)
os.execute("START C:/Users/Administrator/scoop/apps/mpv/0.32.0/mpv.exe " .. proxy_cmd .. " " .. video_url_str)
