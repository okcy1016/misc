local dll_names = {
    'libbrotlicommon.dll', 'libbrotlidec.dll', 'libcrypto-1_1-x64.dll',
    'libcurl-4.dll', 'libidn2-0.dll', 'libnghttp2-14.dll', 'libpsl-5.dll',
    'libssh2-1.dll', 'libssl-1_1-x64.dll', 'libunistring-2.dll', 'lua53.dll',
    'mgwfltknox-1.3.dll'
}

for _, e in ipairs(dll_names) do print(e) end
