cmake_minimum_required(VERSION 3.17)

# use cmake .. -DCMAKE_BUILD_TYPE= or Debug, Release, RelWithDebInfo, MinSizeRel ... for variant build types
project(mpv_http_video_helper VERSION 0.1.1)
# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

find_package(FLTK REQUIRED)
find_package(Lua REQUIRED)
find_package(CURL REQUIRED)

if(CMAKE_BUILD_TYPE STREQUAL "Release" AND WIN32)
    add_executable(mpv_helper WIN32 main.cxx fltk_gui.cxx)
else()
    add_executable(mpv_helper main.cxx fltk_gui.cxx)
endif()
target_include_directories(mpv_helper PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/vendor/cpr/include)
target_include_directories(mpv_helper PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/vendor/sol/include)
target_link_libraries(mpv_helper PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/vendor/cpr/lib/libcpr.a)
target_link_libraries(mpv_helper PRIVATE ${FLTK_LIBRARIES})
target_link_libraries(mpv_helper PRIVATE ${LUA_LIBRARIES})
target_link_libraries(mpv_helper PRIVATE ${CURL_LIBRARIES})

# copy scripts to binary dir after build
add_custom_command(TARGET mpv_helper
                    POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E copy_directory
                        ${CMAKE_CURRENT_SOURCE_DIR}/lua_scripts ${CMAKE_CURRENT_BINARY_DIR}/lua_scripts
)
