#include <iostream>

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>

#include "fltk_gui.hpp"
#include "main.hpp"

struct parse_btn_cb_args
{
    Fl_Window *main_win;
    Fl_Input *url_input;
    std::vector<struct video_info> *video_info_vec;
};

void parse_btn_cb(Fl_Widget *, void *);
void show_parse_result_win(std::vector<struct video_info> *);
void play_video_cb(Fl_Widget *, void *);

int main_gui(int argc, char **argv, std::vector<struct video_info> *video_info_vec)
{
    // set theme
    Fl::scheme("gtk+");

    Fl_Window *window = new Fl_Window(480, 120, "mpv http video helper");
    window->size_range(window->w(), window->h(), 0, 0);
    window->resizable(window);

    int gap_right = 20, gap_hor = 10, gap_left = 100;
    int x = gap_left, y = gap_hor, h_in = 30, h_btn = 30;

    Fl_Input *url_input = new Fl_Input(x, y, window->w() - x - gap_right, h_in, "video_list_url");
    url_input->tooltip("url contains video links");

    y += gap_hor + h_btn;
    Fl_Button *btn_parse = new Fl_Button(x, y, window->w() - x - gap_right, h_btn, "parse");
    parse_btn_cb_args parse_btn_cb_args_struct = {window, url_input, video_info_vec};
    btn_parse->callback(parse_btn_cb, &parse_btn_cb_args_struct);

    window->end();
    window->show(argc, argv);

    return Fl::run();
}

void parse_btn_cb(Fl_Widget *, void *v)
{
    parse_btn_cb_args *parse_btn_cb_args_struct = (parse_btn_cb_args *)v;
    std::string url_str = parse_btn_cb_args_struct->url_input->value();
    parse_url(url_str, parse_btn_cb_args_struct->video_info_vec);
    show_parse_result_win(parse_btn_cb_args_struct->video_info_vec);
    parse_btn_cb_args_struct->main_win->~Fl_Window();
}

void show_parse_result_win(std::vector<struct video_info> *video_info_vec)
{
    Fl_Window *win = new Fl_Window(480, 640, "parse result");
    win->size_range(win->w(), win->h(), 0, 0);
    win->resizable(win);

    int gap_right = 20, gap_hor = 10, gap_left = 20;
    int x = gap_left, y = gap_hor, h_in = 30, h_btn = 30;

    std::vector<Fl_Button *> btn_vec;
    for (std::vector<video_info>::iterator it = video_info_vec->begin(); it != video_info_vec->end(); it++)
    {
        Fl_Button *btn = new Fl_Button(x, y, win->w() - x - gap_right, h_btn, (*it).file_name.c_str());
        btn->callback(play_video_cb, &((*it).url));
        btn_vec.push_back(btn);
        y += gap_hor + h_btn;
    }

    win->end();
    win->show();
}

void play_video_cb(Fl_Widget *, void *v)
{
    std::string url = *((std::string *)v);
    play_video(url);
}
