#include <string>
#include <vector>

#include "sol.hpp"

struct video_info
{
    std::string url;
    std::string file_name;
};

int parse_url(std::string, std::vector<struct video_info> *);
void play_video(std::string);
sol::state init_lua();
std::string http_get_html_str(std::string);
