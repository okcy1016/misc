#include <list>
#include <string>
#include <iostream>

#include "sol.hpp"
#include "cpr/cpr.h"

#include "fltk_gui.hpp"
#include "main.hpp"

int main(int argc, char **argv)
{
    std::vector<struct video_info> video_info_vec;
    main_gui(argc, argv, &video_info_vec);
}

int parse_url(std::string url_str, std::vector<struct video_info> *video_info_vec)
{
    sol::state lua = init_lua();
    lua["url"] = url_str;
    lua.script_file("lua_scripts/video_parser.lua");

    sol::table t_video_info = lua["T_video_info"];
    video_info video_info_struct;
    // std::cout << t_video_info[8].valid() << std::endl;
    for (size_t i = 1; i <= t_video_info.size(); i += 1)
    {
        if (t_video_info[i].valid())
        {
            video_info_struct.file_name = t_video_info[i]["name"];
            video_info_struct.url = t_video_info[i]["url"];
            video_info_vec->push_back(video_info_struct);
        }
        else
        {
            return 1;
        }
        // std::cout << t_video_info[i].valid() << std::endl;
    }
    return 0;
}

void play_video(std::string url)
{
    sol::state lua = init_lua();
    lua["video_url"] = url;
    lua.script_file("lua_scripts/play_video.lua");
}

sol::state init_lua() {
    sol::state lua;
    lua.open_libraries(sol::lib::base, sol::lib::package, sol::lib::string, sol::lib::table, sol::lib::os, sol::lib::io);
    lua.set_function("http_get_html_str", http_get_html_str);
    return lua;
}

std::string http_get_html_str(std::string url) {
    cpr::Response r = cpr::Get(cpr::Url{url});
    return r.text;
}
