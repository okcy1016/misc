#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"


static void error(lua_State *L, const char *fmt, ...);


void load_lua_file(lua_State *L, const char *fname) {
  if (luaL_loadfile(L, fname) || lua_pcall(L, 0, 0, 0))
    error(L, "cannot run lua file: %s", lua_tostring(L, -1));
}

static int find_touchpad_id(lua_State *L) {

  load_lua_file(L, "find_touchpad_id.lua");
  
  int isnum;
  int id;

  // pushing
  lua_getglobal(L, "find_touchpad_id");

  // do the call (0 argument, 1 result)
  if (lua_pcall(L, 0, 1, 0) != LUA_OK)
    error(L, "error running function 'find_touchpad_id': %s\n",
          lua_tostring(L, -1));

  // retrieve result
  id = lua_tonumberx(L, -1, &isnum);
  if (!isnum)
    error(L, "error: function 'find_touchpad_id' should return a number\n");
  lua_pop(L, 1);
  return id;
}

static void error (lua_State *L, const char *fmt, ...) {
  va_list argp;
  va_start(argp, fmt);
  vfprintf(stderr, fmt, argp);
  va_end(argp);
  lua_close(L);
  exit(EXIT_FAILURE);
}

static void stackdump(lua_State * L) {
  int i;
  int top = lua_gettop(L);  /* depth of the stack */
  for (i = 1; i <= top; i++) {  /* repeat for each level */
    int t = lua_type(L, i);
    switch (t) {
    case LUA_TSTRING: {  /* strings */
      printf("'%s'", lua_tostring(L, i));
      break;
    }
    case LUA_TBOOLEAN: {  /* Booleans */
      printf(lua_toboolean(L, i) ? "true" : "false");
      break;
    }
    case LUA_TNUMBER: {  /* numbers */
      printf("%g", lua_tonumber(L, i));
      break;
    }
    default: {  /* other values */
      printf("%s", lua_typename(L, t));
      break;
    }
    }
    printf("  ");  /* put a separator */
  }
  printf("\n");  /* end the listing */
}
    
int main(void) {
  lua_State * L = luaL_newstate();
  luaL_openlibs(L);
    
  printf("touchpad id: %d\n", find_touchpad_id(L));
    
  lua_close(L);
  return 0;
}
