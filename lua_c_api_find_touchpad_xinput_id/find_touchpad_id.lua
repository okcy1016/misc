function split(str, pat)
   local t = {}
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end


function find_touchpad_id()
   local file_handle = io.popen("cat a.txt")
   local out_str = file_handle:read("*a")
   file_handle:close()

   res_table = split(out_str, "\n")
   ---- finding
   -- spliting
   for _, res_per_line in ipairs(res_table) do
      -- print(res_per_line)
      if not string.find(res_per_line, "mouse") then
         goto continue
      end
      for _, res_split_comma in ipairs(split(res_per_line, ",")) do
         res_split_comma = string.gsub(res_split_comma, "%s+", "")
         res_split_equal = split(res_split_comma, "=")
         for i, val in ipairs(res_split_equal) do
            if val == "id" then
               return tonumber(res_split_equal[i+1])
            end
         end
      end
      ::continue::
   end
end

-- print(find_touchpad_id())
