## load xhtml
# get file names
home_dir = "/home/okcy"
f_dir = [
    "/Documents/_/0",
    "/Documents/_/1",
    "/Documents/_/2",
    "/Documents/_/3",
    "/Documents/_/4"
]
for i, _ in enumerate(f_dir):
    f_dir[i] = home_dir + f_dir[i]

# print(f_dir)
import os
file_names = []
for item in f_dir:
    sub_file_names = os.listdir(item)
    sub_file_names.sort()
    for e in sub_file_names:
        file_names.append(item + "/" + e)

# print(file_names)
        
# parse for plain text
from bs4 import BeautifulSoup
text_list = []
for n in file_names:
    with open(n, 'r') as fpr:
        text_list.append(BeautifulSoup(fpr, 'html.parser').text)

# print("leng: ", len(text_list))
# print("first elem: ", text_list[0])

# concatenate plain text to one file
with open("/tmp/__one_sentence_helper_out.txt", "a") as fpw:
    for txt in text_list:
        fpw.write(txt)
