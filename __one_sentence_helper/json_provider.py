# load text content
import sys
try:
    f_path = sys.argv[1]
    # port = sys.argv[2]
except:
    print("not enough args")
    exit(1)
    # print(f_path)

# func def
import random
def get_sentence(text_list):
    delimiters = ["\n", "：", "。", "？", "！", "，", "、", " ", "；"]
    r_s = ""
    min_s_len = 16
    i = 0
    while True:
        # generate random num
        random.seed()
        i = random.randint(0, len(text_list) - 1)
        # prepare
        if text_list[i] not in delimiters:
            break
        
    r_s = text_list[i]
    while len(r_s) < min_s_len:
        # check bounder
        if i == len(text_list) - 1:
            break
        i += 1
        r_s += text_list[i]
    # shape end
    while r_s[-1] in delimiters:
        r_s = r_s[:-1]
    r_s = r_s.strip()

    # print(r_s)
    return r_s


import json
def gen_and_upload_f():
    text_s = ""
    with open(f_path, "r") as fpr:
        text_s = fpr.read()        
        # print(len(text_s))
        
    # split text into sentences
    import re
    all_text_list = re.split(r"(\n|：|。|？|！|，|、|；| )", text_s)
    # print(len(text_list))
    
    text_amount = 512
    text_list = []
    # construct and write file
    for _ in range(text_amount):
        text_list.append(
            {
                "text": get_sentence(all_text_list),
                "by": "",
                "from": "",
                "time": [""]
            }
        )
    print(len(text_list))
    j_s = json.dumps(text_list, ensure_ascii=False)
    with open("out.json", "w") as fpw:
        fpw.write(j_s)

    # upload
    os.system("git commit -am 'update'")
    os.system("git push")
    

# init gitee json storage repo
import os
if not os.path.exists("/tmp/onetext_helper"):
    os.chdir("/tmp")
    os.system("git clone https://gitee.com/oknil/onetext_helper")
os.chdir("/tmp/onetext_helper")
# print(os.system("ls"))
os.system("git pull")


import time
while True:
    gen_and_upload_f()
    time.sleep(1843200) # 512h
    

# # serve api
# from flask import Flask, send_from_directory
# app = Flask(__name__)    
    
# @app.route("/__one_sentence_helper")
# def helper():
#     # construct and write file
#     j_s = json.dumps(
#         [{
#             "text": get_sentence(),
#             "by": "",
#             "from": "",
#             "time": [""]
#         }],
#         ensure_ascii=False
#     )
#     with open("out.json", "w") as fpw:
#         fpw.write(j_s)
#     return send_from_directory(".", "out.json")

# app.run(host = "0.0.0.0", port = port)
