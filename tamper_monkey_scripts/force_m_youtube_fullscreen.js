// ==UserScript==
// @name         force_m_youtube_fullscreen
// @namespace    https://github.com/okcy1016
// @version      0.2
// @description  force fullscreen when playing videos on 'm.youtube.com'
// @author       okcy
// @match        https://m.youtube.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function toggleFullscreen() {
        // find video elem
        var videoElem = document.getElementById("player-container-id");
        // fullscreen it
        if (!document.fullscreenElement) {
            videoElem.requestFullscreen().catch(err => {
                alert(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`);
            });
        } else {
            document.exitFullscreen();
        }
    }

    // add listener of fullscreenchange
    document.addEventListener('fullscreenchange', (event) => {
        // entering fullscreen
        if (document.fullscreenElement) {
            // const curOriType = screen.orientation.type;
            // const isPortrait = curOriType.includes("portrait");
            // landscape it
            const nextOriType = "landscape-primary";
            const p = screen.orientation.lock(nextOriType);
        }
    });
})();