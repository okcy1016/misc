// ==UserScript==
// @name         [Bilibili] dont fuxxing turn on danmu on default!
// @namespace    https://gitlab.com/okcy1016
// @version      0.2
// @description  disable bilibili av danmu by after page loaded
// @author       okcy
// @include      *://www.bilibili.com/video/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function turnOffDanmu() {
        var danmuCheckBox = document.querySelector(".bilibili-player-video-danmaku-switch > input:nth-child(1)");
        if (danmuCheckBox == null) {
            setTimeout(function() {
                turnOffDanmu();
            }, 500);
        } else {
            if (danmuCheckBox.checked) {
                danmuCheckBox.click();
            }
        }
    }

    console.log("turning off danmu ... [by okcy]");
    turnOffDanmu();
})();
